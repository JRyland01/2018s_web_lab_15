<%@ page import="ictgradschool.web.lab15.ex1.AccessLog" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Exercise 01</title>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
    <style>
        #myTable {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #myTable td, #customers th {
            border: 1px solid plum;
            padding: 8px;
        }

        #myTable tr:nth-child(even){background-color: lightcyan;}

        #myTable tr:hover {background-color: hotpink;}

        #myTable th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: black;
            color: white;
        }
    </style>
</head>
<body>
<form action="/question1/new" method="POST">
    <label for="name" >Name:</label>
    <input type="text" autofocus="autofocus" size="100" name="name" id="name"
           placeholder="Your name goes here..."/>
    <br>
    <label for="description" >Name:</label>
    <input type="text" autofocus="autofocus" size="100" name="description" id="description"
           placeholder="Your Description goes here..."/>
    <br>
    <input type="submit" value="Submit">
</form>
<table id="myTable">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Time</th>
    </tr>
    <c:forEach items="${AccessLogList}" var="AccessLog">
        <tr>
            <td>${AccessLog.id}</td>
            <td>${AccessLog.getName()}</td>
            <td>${AccessLog.getDescription()}</td>
            <td>${AccessLog.getTimestamp()}</td>
        </tr>
    </c:forEach>
    <c:if test="${AccessLogList.size() >30}" >
        <tfoot>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Time</th>
        </tr>
        </tfoot>
    </c:if>
</table>
</body>
</html>
