package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class LoggingTableNewEntry extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: Retrieve parameters and store new entries in the database
        // TODO: Redirect back to the LoggingTable
        try (Connection conn = DBConnection.createConnection()){
            AccessLogDAO dao = new AccessLogDAO(conn);
            AccessLog a = new AccessLog();
            a.setName(request.getParameter("name"));
            a.setDescription(request.getParameter("description"));
            dao.addAccessLog(a);
            request.getRequestDispatcher("../question1").forward(request,response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }
}
