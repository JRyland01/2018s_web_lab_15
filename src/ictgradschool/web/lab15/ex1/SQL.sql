DROP TABLE IF EXISTS access_logs;

CREATE TABLE access_logs (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) ,
  description VARCHAR(250),
  timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);
  INSERT INTO access_logs (name, description) VALUES
    ('Frodo', 'Carries the Ring'),
    ('Aragon','Carries the fellowhship'),
    ('Legolas', 'Carries his bow');

