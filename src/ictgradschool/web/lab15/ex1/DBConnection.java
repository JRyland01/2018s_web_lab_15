package ictgradschool.web.lab15.ex1;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {

    public static Connection createConnection() throws SQLException {

        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        Properties dbProps = new Properties();


        try(InputStream fIn = Thread.currentThread().getContextClassLoader().getResourceAsStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return DriverManager.getConnection(dbProps.getProperty("url"), dbProps);

    }

    private static Properties loadProperties() {

        try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("mysql.properties")) {

            Properties props = new Properties();
            props.load(in);
            return props;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

}