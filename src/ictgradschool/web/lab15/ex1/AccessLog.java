package ictgradschool.web.lab15.ex1;

import java.io.Serializable;
import java.sql.Timestamp;

public class AccessLog implements Serializable {
    private int id;
    private String name;
    private String description;
    private Timestamp Timestamp;

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public java.sql.Timestamp getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(java.sql.Timestamp timestamp) {
        Timestamp = timestamp;
    }
}


